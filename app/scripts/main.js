var PAGE = {
    init: function(){
        // Pickadate
        jQuery.extend( jQuery.fn.pickadate.defaults, {
            monthsFull: [ 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre' ],
            monthsShort: [ 'ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic' ],
            weekdaysFull: [ 'domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado' ],
            weekdaysShort: [ 'dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb' ],
            today: 'hoy',
            clear: 'borrar',
            close: 'cerrar',
            firstDay: 1,
            format: 'dddd d !de mmmm !de yyyy',
            formatSubmit: 'yyyy/mm/dd'
        });

        // Sticky toolbar
        $('.js-fixedsticky').fixedsticky();

        //Init responsive tables
        new PAGE.ResponsiveTable();
        new PAGE.Global();
    }
};






// Responsive Tables
PAGE.ResponsiveTable = function(){
    this.breakpoint = 800;
    this.switched   = false;
    this.init();

    $(window).on('resize', this.update.bind(undefined, this));
};

PAGE.ResponsiveTable.prototype.init = function() {
    this.update(this);
};

PAGE.ResponsiveTable.prototype.update = function(that) {
    if (($(window).width() < that.breakpoint) && !that.switched ){
      that.switched = true;
      $('table.responsive').each(function(i, element) {
        that.split($(element));
      });
      return true;
    }
    else if (that.switched && ($(window).width() > that.breakpoint)) {
      that.switched = false;
      $('table.responsive').each(function(i, element) {
        that.unsplit($(element));
      });
    }
};

PAGE.ResponsiveTable.prototype.split = function($element) {
    $element.wrap('<div class=\'table-wrapper\' />');

    var copy = $element.clone();
    copy.find('td:not(:first-child), th:not(:first-child)').css('display', 'none');
    copy.removeClass('responsive');

    $element.closest('.table-wrapper').append(copy);
    copy.wrap('<div class=\'pinned\' />');
    $element.wrap('<div class=\'scrollable\' />');

    copy.parent().width(copy.width());
    $element.parent().css('margin-left', copy.width());

    this.setCellHeights($element, copy);
};

PAGE.ResponsiveTable.prototype.setCellHeights = function($element, copy) {
    var tr = $element.find('tr'),
        tr_copy = copy.find('tr'),
        heights = [];

    tr.each(function (index) {
      var self = $(this),
          tx = self.find('th, td');

      tx.each(function () {
        var height = $(this).outerHeight(true);
        heights[index] = heights[index] || 0;
        if (height > heights[index]) heights[index] = height;
      });

    });

    tr_copy.each(function (index) {
      $(this).height(heights[index]);
    });
};

PAGE.ResponsiveTable.prototype.unsplit = function($element) {
    $element.closest('.table-wrapper').find('.pinned').remove();
    $element.unwrap();
    $element.unwrap();
};






// GLOBAL PAGE METHODS
PAGE.Global = function(){
  this.init();
};

PAGE.Global.prototype.init = function() {
  this.checkNewRequirementOnMobile();
};

/**
 * Checks if we're in the new requirement window,
 * if so, on mobile shows the form to add a new requirement.
 */
PAGE.Global.prototype.checkNewRequirementOnMobile = function() {
  var isItNewRequirement = (document.getElementById('agregar') != null) ? true : false;
  var toolBars           = {
    list: document.querySelector('[data-section="listado"]'),
    add : document.querySelector('[data-section="agregar"]')
  };
  var locationHash       = window.location.hash;
  var check              = function(){
    var windowWidth = window.innerWidth;

    if(isItNewRequirement === true && windowWidth < 981){
      window.location.hash = '#agregar';
      toggleToolBars();
    }else{
      window.location.hash = '';
      resetToolBars();
    }
  };
  var toggleToolBars = function(){
    toolBars.add.classList.toggle('u-hidden-visually');
    toolBars.list.classList.toggle('u-hidden-visually');
  };
  var resetToolBars = function(){
    toolBars.add.classList.add('u-hidden-visually');
    toolBars.list.classList.remove('u-hidden-visually');
  };


  if (isItNewRequirement === true) {
    window.addEventListener('resize', check);
    window.addEventListener('DOMContentLoaded', check);
    window.addEventListener('hashchange', toggleToolBars);
    window.addEventListener('unload', resetToolBars);
  };
};



PAGE.init();
